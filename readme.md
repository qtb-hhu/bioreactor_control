# Open Fermenter Server (OFS)
A server software to collect the data from various Open Fermenter modules.

## Legal information
Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Functionalities
The prototype comes with following functionalities:

* [x] pulling data from Arduino based devices
* [x] logging data to a CSV-file
* [x] adding manual entries to the logfile
* [x] viewing of CSV entries
* [x] correction of CSV entries
* [x] passing plotting of data to external scripts (Python, R)

## Files
### Program files
| File | Function |
| --- | --- |
| install.py | Downloads external ressources |
| main.py | Starts the programm |
| device.py | Provides the device class to communicate with OF modules |
| logger.py | Provices a virtual lab book to log data to a CSV |
| gui.py | Simple GUI for fermenter setup, data editing and plotting |
| config.yaml | Configuration file for permanent installations |

### External resources
| File | Function | Source |
| --- | --- | --- |
| clock.py | Enables a content manager for timing blocks of code | [GitLab](https://gitlab.com/snippets/1663081/raw) |
| fileedit.py | Enables a content manager for editing files | [GitLab](https://gitlab.com/snippets/1663140/raw) |
| perpetualtimers.py | Enables a recursively recurring timer | [GitLab](https://gitlab.com/snippets/1663125/raw) |
| unbuffered_linecount.py | Provides an easy on memory line count | [GitLab](https://gitlab.com/snippets/1663307/raw) |