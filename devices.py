#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
Device class for the server side control of the Open Fermenter modules.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from perpetualtimers import PeriodicalTimer
import serial
from time import time

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Prototype'
__version__ = '0.1.6'  # main version, minor version, revision


class Device:
    def __init__(self, device_id, serial_connection, reference_time, measurement_interval, line_format_fn, **carryover):
        """
        Initiates the software side representation of a device.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.

        serial_connection: dict
            Keyword parameters to be passed onto serial.Serial instance.

        reference_time:
            UNIX time when the device was started. This is added to the devices runtime such that always a UNIX time is
            returned.

        measurement_interval: float
            Interval of the device loop.

        format_fn: function
            Function used tpoformat the raw output of the device. Expected to accept a string and return a tuple of
            length three.

        carryover: **kwargs
            Sink for unused keyword parameters. Required for pythons multiple inheritance.
        """
        # Device meta data
        self.id = device_id
        self.serial_connection = serial_connection
        self.t0 = reference_time
        self.measurement_interval = measurement_interval

        # function for reformatting lines lines prior returning them
        # (these tend to be device specific, thus including this step here rather than downstream is more practicable)
        self.format_fn = line_format_fn
        if not callable(self.format_fn):
            import format_fns  # lazy evaluation
            try:
                self.format_fn = getattr(format_fns, self.format_fn)
            except (AttributeError, TypeError):
                raise ValueError("'format_fn' needs to be a function accepting a string an returning a list of tuples "
                                 "of length three, or the name of a standard function (see module format_fns)")

    def _request(self, cmd, timeout_read=0, timeout_write=0):
        """
        Internal function for the raw communication.

        Parameters
        ----------
        cmd: str
            Command sent the the Arduino based device.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.

        Returns
        -------
        str
            unprocessed answer of the Arduino based device.
        """
        try:
            with serial.Serial(**self.serial_connection, timeout=timeout_read, write_timeout=timeout_write) as s:
                s.write(bytes(cmd, 'utf-8'))
                ans = s.readline()

        except ValueError:
            raise ValueError

        except serial.SerialException:
            raise BrokenPipeError

        if not ans:
            raise EOFError
        else:
            return ans

    def communicate(self, cmd, strikes=3, timeout_read=0, timeout_write=0):
        """
        Function to execute the communication request and process the answer.

        Parameters
        ----------
        cmd: str
            Command sent the the Arduino based device.

        strikes: int or numpy.inf
            Number of tries before communication is deemed as failed. Defaults to 3.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.

        Returns
        -------
        tuple: (str, int, arbitrary)
            Key, runtime and value as a tuple.
        """
        k = self.id + '_' + str(cmd).lower()
        try:
            ans = self._request(cmd, timeout_read, timeout_write)

        except serial.SerialTimeoutException:
            if strikes > 0:
                self.communicate(cmd, strikes - 1, timeout_read, timeout_write)

            return [(k, time(), 'Connection timeout')]

        except BrokenPipeError:
            if strikes > 0:
                self.communicate(cmd, strikes - 1, timeout_read, timeout_write)

            return [(k, time(), 'Connection broken')]

        except EOFError:
            if strikes > 0:
                self.communicate(cmd, strikes - 1, timeout_read, timeout_write)

            return [(k, time(), 'No data received')]

        except ValueError:
            return [(k, time(), 'invalid serial configuration')]

        else:
            entries = []
            for k, t, v in self.format_fn(ans):
                k = self.id + '_' + k

                try:
                    t = float(t) + self.t0
                except ValueError:
                    t = 't_dev+ref_{}+{}'.format(t, str(self.t0))

                entries.append((k, t, v))

            return entries

    def pull(self, strikes=3, timeout_read=0, timeout_write=0):
        """
        Alias for communicate('PULL',...)

        Parameters
        ----------
        strikes: int or numpy.inf
            Number of tries before communication is deemed as failed. Defaults to 3.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.

        Returns
        -------
        tuple: (str, int, arbitrary)
            Key, runtime and value as a tuple.
        """
        # Arduino should return a data identification key, the device_runtime and the date on successful PULL
        return self.communicate('PULL', strikes, timeout_read, timeout_write)

    def reset(self, reference_time=None, strikes=3, timeout_read=0, timeout_write=0):
        """
        Sends a command for device side reset to the Arduino. While control will reset it's state to active and store
        a new reference time.

        Parameters
        ----------
        reference_time: float
            New reference time for device after reset. If set to None, the old time will be reused. Defaults to None

        strikes: int or numpy inf
            Number of tries before communication is deemed as failed. Defaults to 3.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.

        Returns
        -------
        tuple: (str, int, 'reset successful')
            Key, runtime and the string 'reset successful' as a tuple.
        """
        # Arduino should return, '_meta', device_runtime and a string saying 'reset successful' on successful RESET
        k, t, v = self.communicate('RESET', strikes, timeout_read, timeout_write)[0]
        if v == 'reset successful' and reference_time is not None:
                self.t0 = reference_time


class DeviceTimer(Device, PeriodicalTimer):
    """
    Device class inheriting from perpetualtimers.PeriodicalTimer class.
    Uses the threading abilities to periodically pull data from the Arduino based device but will also allow for manual
    intervention as with the normal Device class.
    """
    def __init__(self, data_queue, device_id, serial_connection, reference_time, measurement_interval, format_fn,
                 strikes=3, timeout_read=0, timeout_write=0):
        """
        Initiates the software side representation of a device as a variant of a Timer thread.

        Parameters
        ----------
        data_queue: queue.Queue
            Queue for data to be written to the log.

        device_id: str
            Software side identifier for the device.

        serial_connection: dict
            Keyword parameters to be passed onto serial.Serial instance.

        reference_time:
            UNIX time when the device was started. This is added to the devices runtime such that always a UNIX time is
            returned.

        measurement_interval: float
            Interval of the device loop.

        format_fn: str or function
            Function or name of standard function used for formatting the raw device output.

        strikes: int or numpy.inf
            Number of tries before communication is deemed as failed. Defaults to 3.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.
        """
        super().__init__(device_id, serial_connection, reference_time, measurement_interval, format_fn)
        PeriodicalTimer.__init__(self, self.measurement_interval, self._process, dt_correction=True, daemonic=True)

        self.out_queue = data_queue
        self.strikes = strikes
        self.timeout_read = timeout_read
        self.timeout_write = timeout_write

    def _process(self):
        """ Internal Method providing the function for the run method of the PeriodicalTimer part. """
        for ktv in self.pull(strikes=self.strikes, timeout_read=self.timeout_read, timeout_write=self.timeout_write):
            self.out_queue.put(ktv)

if __name__ == '__main__':
    print("Open Fermenter Device Class Copyright (C) 2017  Heinrich Heine University\n"
          "This program comes with ABSOLUTELY NO WARRANTY; for details see documentation.\n"
          "This is free software, and you are welcome to redistribute it under certain conditions;"
          "see 'license.txt' for details.\n")
