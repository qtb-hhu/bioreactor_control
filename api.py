#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
API for a simple server application controlling the Open Fermenter modules.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import atexit
import csv
import devices
import logger
from numbers import Number
from numpy import inf
import os
from queue import Queue
from tempfile import NamedTemporaryFile
from time import time
from unbuffered_linecount import unbuffered_linecount

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Prototype'
__version__ = '0.1.10'  # main version, minor version, revision


class API:
    """
    Context manager for the main program loop. Will ensure that the program ends in an orderly fashion (e. g. stopping
    devices, waiting for the threads to join, etc.). Also acts as a wrapper for device commands and pairs them with log
    entries.
    """

    def __init__(self, file_path, csv_delimiter=';', t0=None):
        """
        Initiates the log files and data writing queue.

        Parameters
        ----------
        file_path: str
            File path to the log file. If it doesn't exists jet it will be created.
        """
        self.file_path = file_path
        temp_file = NamedTemporaryFile('a', encoding='utf-8', newline='', suffix='.csv', delete=False)
        temp_file.close()
        self._temp_path = temp_file.name
        self.meta_log = logger.Log('meta_log', self._temp_path, csv_delimiter=csv_delimiter, t0=t0)
        self.devices = {}

        # Creating log queue (also functioning as buffer)
        self.log_queue = Queue()  # use maxsize=0 to allow for endless buffering

        # spawn threads to print
        self.data_log = logger.LogThread(self.log_queue, 'data_log', self.file_path, csv_delimiter=csv_delimiter, t0=t0)
        self.data_log.setDaemon(True)
        self.data_log.start()

        # Register __exit__ as atexit task to ensure that it is called even if API is not used as context manager
        atexit.register(self.__exit__, None, None, None)

    def __enter__(self):
        """ Returns the instance of the Main context manager. """
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Leaves the context manager and ensures that all data is written to the permanent file and that the temp file is
        closed afterwards.

        Parameters
        ----------
        exc_type
        exc_val
        exc_tb
        """
        # Unregister __exit__ as atexit task to avoid calling it twice
        atexit.unregister(self.__exit__)

        # Stop and kill all device threads
        devices = [device_id for device_id in self]
        for device_id in devices:
            self.cancel(device_id)
            print("Shutdown device with ID '{}'.".format(device_id))

        # wait until all data is written
        print("waiting for data buffer to empty out...")
        self.log_queue.join()

        # now close logs, reopen them and append meta log to data log prior deleting temp file used for meta log
        self.meta_log.__exit__(None, None, None)  # this is ok because nothing is sacred in python...
        self.data_log.__exit__(None, None, None)  # ..calling __exit__ explicitly will just execute the code within.
        with open(self._temp_path, 'r') as meta, open(self.file_path, 'a') as data:
            # using csv because it will handle all the weird edge cases where a delimiter occurs in the data to write
            data = csv.writer(data, delimiter=self.data_log.delimiter)
            meta = csv.reader(meta, delimiter=self.meta_log.delimiter)
            for k, t, v in meta:
                data.writerow([k, t, v])

        os.unlink(self._temp_path)
        assert not os.path.exists(self._temp_path)

    def __getitem__(self, key):
        """ Returns the Devices as items of self."""
        return self.devices[key]

    def __iter__(self):
        """ Yields the stored devices IDs on iteration over the instance. """
        for device_id in self.devices:
            yield device_id

    # thin wrappers for DeviceTimer (for convenience only)
    def spawn(self, device_id, serial_connection, reference_time, measurement_interval, line_format_fn, strikes,
              timeout_read, timeout_write):
        """
        Spawns an instance of the DeviceTimer which periodically collects new data and accepts manual commands.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.

        serial_connection: dict
            Keyword parameters to be passed onto serial.Serial instance.

        reference_time:
            UNIX time when the device was started. This is added to the devices runtime such that always a UNIX time is
            returned.

        measurement_interval: float
            Interval of the device loop.

        line_format_fn: str or function
            Function or name of standard function used for formatting the raw device output.
        """
        self.devices[device_id] = devices.DeviceTimer(self.log_queue, device_id, serial_connection, reference_time,
                                                      measurement_interval, line_format_fn, strikes, timeout_read,
                                                      timeout_write)
        self.log_queue.put([device_id+'_meta', time(), 'initiation'])

    def spawn_from_config(self, config_yaml='config.yaml', reference_time=None):
        """
        Method to spawn the Open Fermenter control threads by parsing a yaml file.

        Parameters
        ----------
        config_yaml: str
            File path to the yaml formatted config file. See config.yaml for an example.

        reference_time: float or None
            A UNIX time as reference time for all devices. If set None, the current time will be used. Defaults to None.
        """
        import yaml  # lazy evaluation

        if reference_time is None:
            reference_time = time()

        if os.path.isfile(config_yaml):
            with open(config_yaml, 'r') as f:
                conf = yaml.load_all(f)
                for device in conf:
                    self.spawn(reference_time=reference_time, **device)

    def start(self, device_id):
        """
        Thin wrapper to start a device. Also creates a log entry.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.
        """
        self[device_id].start()
        self.log_queue.put([device_id + '_meta', time(), 'started'])

    def pause(self, device_id):
        """
        Thin wrapper to pause a device. Also creates a log entry.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.
        """
        self[device_id].pause()
        self.log_queue.put(self[device_id].communicate('PAUSE'))
        self.log_queue.put([device_id + '_meta', time(), 'thread paused'])

    def resume(self, device_id):
        """
        Thin wrapper to resume a paused a device. Also creates a log entry.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.
        """
        self[device_id].resume()
        self.log_queue.put(self[device_id].communicate('RESUME'))
        self.log_queue.put([device_id + '_meta', time(), 'thread resumed'])

    def query(self, device_id, cmd, timeout_read=0, timeout_write=0):
        """
        Thin wrapper to query a device. Also creates a log entry.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.

        cmd: str
            Command sent the the Arduino based device.

        timeout_read: float
            Seconds. Timer until read method of serial connection will fail. Defaults to 0.

        timeout_write: float
            Seconds. Timer until write method of serial connection will fail. Defaults to 0.
        """
        self[device_id]._request(cmd, timeout_read, timeout_write)
        self.log_queue.put([device_id + '_cmd', time(), cmd])

    def cancel(self, device_id):
        """
        Thin wrapper to cancel a device. Also creates a log entry.

        Parameters
        ----------
        device_id: str
            Software side identifier for the device.
        """
        self[device_id].cancel()
        self.devices.pop(device_id)
        self.log_queue.put([device_id + '_meta', time(), 'stopped'])

    # Thin wrapper for manual logging (for convenience only)
    def user_input_evaluation(self, k, t, v, add_key=False):
        """
        Basic validation of user input. Use this function to wrap your input if you attempt to use the core methods of
        the Log class directly.

        Parameters
        ----------
        k: str or None
            Key for the entry. Ignores k=None because logger.Log.replace_row method will assume that the old key is to
            be reused if no new key is given.

        t: float or None
            Timestamp. Expects seconds from start of the device. Will assume current time if set to None.

        v: arbitrary
            Log entry. Most likely a string.

        add_key: bool
            If set True, a non-existing key will be added else and error will be raised.

        Raises
        ------
        ValueError
            If Time or Value input is invalid.

        KeyError
            If Key input is invalid (doesn't exists, or already exists on attempt to add as a new key.)
        """
        if not (t is None or isinstance(t, Number)):
            raise ValueError("Time 't' needs to be numerical or None!")

        if v is None or v.strip() == '':
            raise ValueError("Value 'v' needs to be non empty!")

        if k is not None:
            keys = self.meta_log.extract_keys()
            if (add_key and k not in keys) or k in keys:  # ..or (not add_key and ...) is implied
                pass  # check was easier to construct this way round
            elif add_key:  # ..and key in keys ... is implied
                raise KeyError("Key '{}' already exists.\nCurrently known keys are'{}'.".format(k, "', '".join(keys)))
            else:  # not add_key and key not in keys ... is implied
                raise KeyError("Key '{}' does not exist. Use existing key or change mode!\n"
                               "Currently known keys are '{}'.".format(k, "', '".join(keys)))

        # return key, time and value if no error was raised.
        return k, t, v

    def append(self, key, time=None, value=None, add_key=False):
        """
        Thin wrapper to manually add an entry to the meta log with basic input validation (calls
        self.user_input_validation prior passing call on to self.meta_log.append).

        Parameters
        ----------
        key: str
            Key for the entry.

        time: float or None
            Timestamp. Expects seconds from start of the device. Will assume current time if set to None.
        value: arbitrary
            Log entry. Most likely a string.

        add_key: bool

        Raises
        ------
        ValueError
            If Time or Value input is invalid.

        KeyError
            If Key input is invalid (doesn't exists, or already exists on attempt to add as a new key.)
        """
        if key is None:
            raise ValueError("Key cannot be None!")
        self.meta_log.append(*self.user_input_evaluation(key, time, value, add_key))

    def correct_last(self, key, time=None, value=None, add_key=False):
        """
        Replaces the last line added. Add lines written at an earlier state use the core methods if the logger.Log
        class (e.g. by determining the line number with a loop and then calling logger.Log.replace_row).

        Parameters
        ----------
        key: str or None
            Key for the entry. Will assume that the old key is to be reused if no new key is given.

        time: float or None
            Timestamp. Expects seconds from start of the device. Will assume current time if set to None.

        value: arbitrary
            Log entry. Most likely a string.

        add_key: bool
            If set True, a non-existing key will be added else and error will be raised.

        Raises
        ------
        ValueError
            If Time or Value input is invalid.

        KeyError
            If Key input is invalid (doesn't exists, or already exists on attempt to add as a new key.)
        """
        k, t, v = self.user_input_evaluation(key, time, value, add_key)
        self.meta_log.replace_row(unbuffered_linecount(self.meta_log.file), k, t, v)

    def runtime(self):
        """ Returns the current runtime of the data log. """
        return self.data_log._tsp()
