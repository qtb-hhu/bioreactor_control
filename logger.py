#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
Simple data logger for the server application of the Open Fermenter modules.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import csv
from fileedit import FileEdit
from io import StringIO
from itertools import islice
from jellyfish import damerau_levenshtein_distance
import os
from threading import Thread
from time import time
from unbuffered_linecount import unbuffered_linecount

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Prototype'
__version__ = '0.1.13'  # main version, minor version, revision


class Log:
    """
    Data logger class for the Open Fermenter Server application.
    The Log class can be used as standalone program to function as virtual lab book.
    Pythons native 'with' statement is supported, thought, it might be impractical due to Pythons GIL.

    Examples
    --------
    # Usage as a normal class instance
    log = Log('OFS-1', 'log.csv', ';')
    log.append('meta', time.time(), 'Entry no. 1')
    log.append('meta', time.time(), 'Entry no. 2')
    log.append('meta', time.time(), 'Entry no. 3')
    del log  # make sure object is garbage collected to append "Log closed." entry

    # Usage as a context manager
    with Log('OFS-2', 'log.csv', ';') as log:
        log.append('meta', time.time(), 'Entry no. 1')
        log.append('meta', time.time(), 'Entry no. 2')
        log.append('meta', time.time(), 'Entry no. 3')
    """
    def __init__(self, log_id, file_path, csv_delimiter=';', t0=None, header=False, **carryover):
        """
        Initiates instance of the Log object.
        
        Parameters
        ----------
        log_id: str
            Identifier to help clearly distinguish between different Log instances.
            
        file_path: str
            File path (including filename) of the log file. Non-existent files and/or directories are created.
            
        csv_delimiter: str
            Single character delimiter of the CSV dialect used. Defaults to ';'.
            This will be ignored if an existing log file is re-opened (the same that is already used will be use again).

        t0: float or None
            Start time of the log (epoch time) or None to use the current time. Defaults to None.

        header: bool
            If set True, a header will be added every time a log is opened.

        carryover: **kwargs
            Sink for unused keyword parameters. Required for pythons multiple inheritance.
        """
        self.id = str(log_id)
        self.file = os.path.abspath(os.path.expanduser(os.path.normpath(file_path)))
        self._with = False  # flag for the garbage collector action

        self.t0 = time() if t0 is None else t0

        # checking for path and file, creating them if necessary
        if not os.path.exists(os.path.dirname(self.file)):
            os.makedirs(os.path.dirname(self.file))

        if header:
            self.append('key', 'time', 'value')

        if os.path.exists(self.file):
            # path exists and file exists: sniff out csv dialect and append note that file was re-opened as a log
            try:
                with open(self.file, 'r') as f:
                    self.delimiter = csv.Sniffer().sniff(f.readline()).delimiter
                self.append('meta', self._tsp(), 're-opened with new log ID ')
            except csv.Error:  # in case of an empty tempfile csv.Sniffer will fail, thus, use standard delimiter
                self.delimiter = ';'

        else:
            # path exists or is created and file does not exist: create file with given delimiter and add header
            self.delimiter = csv_delimiter

        # general header line added every time a log is opened
        self.append('log_id', self._tsp(), self.id)

    def __enter__(self):
        """
        Returns the instance of the context manager, obtainable via the 'as' keyword of the 'with' statement, allowing
        to work with the instance but ensuring that all meta entries are really added.

        Notes
        -----
        It is perfectly fine to use the Log class without using Pythons native 'with' statement, however, make sure that
        the instance is garbage collected after you are done. e.g. by deleting all references to the instance via
        Pythons 'del' statement.

        Returns
        -------
        self
            Returns the instance of the Log object, allowing to use its methods but ensuring that the log will be closed
            properly after all data was written.
        """
        self._with = True  # flag for the garbage collector action
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Ensure that a "closed log." entry is appended to the log if it is closed in context manager mode.

        Parameters
        ----------
        exc_type
        exc_val
        exc_tb
        """
        self.append('meta', self._tsp(), 'Closed log.')

    def __del__(self):
        """
        Destructor called during garbage collection.
        Last ditch-attempt to append a 'Closed log.' entry if it has not been done yet.
        """
        if not self._with:
            self.append('meta', self._tsp(), 'Closed log.')

    # Internal helper functions
    def _tsp(self):
        """
        Timestamp function.

        Returns
        -------
        float
            time.time() - self.t0
        """
        return time() - self.t0

    # Log writing functions (and pseudo writing)
    def append(self, k, t, v):
        """
        Appends raw input to log file.
        
        No error checking or input validation is done. This method should be called by other program internals
        exclusively and almost never by the user directly. However, in some rare instances it is convenient to force the
        addition of a row by calling append manually.
         
        Parameters
        ----------
        k
            Key for 1st. column.
        t
            Time stamp for 2nd. column. Will assume current time if set to None.
        v
            Value or Text for 3rd. column.
        """
        if t is None:
            t = self._tsp()

        with open(self.file, 'a', newline='') as f:
            w = csv.writer(f, delimiter=self.delimiter)
            w.writerow([k, t, v])

    def dummy(self, k, t, v):
        """
        Like append but returns csv formatted line instead of actually writing it to the log file.

        This method is used by the replace_row and replace_pattern methods which internally expect a string rather than
        a csv.writer action. just joining the data using self.delimiter als connector might work for most cases but this
        ensures that all edge cases are handled exactly the same way csv.writer.write would handle them.

        Parameters
        ----------
        k
            Key for 1st. column.
        t
            Time stamp for 2nd. column.
        v
            Value or Text for 3rd. column.
        """
        with StringIO(newline='') as d:
            csv.writer(d, delimiter=self.delimiter).writerow([k, t, v])
            return d.getvalue()

    # Log manipulation functions
    def replace_row(self, l, k=None, t=None, v=None):
        """
        Replaces the row specified with the row generated from k, t, and v.
        
        Notes
        -----
        This method will iterate over several file object iterators.
        
        Parameters
        ----------
        l: int
            Line number of line to replace.
        k
            Replacement for key, will keep old key if set to None. Defaults to None. 
        t
            Replacement for time, will keep old time if set to None. Defaults to None.
        v
            Replacement for value, will keep old value if set to None. Defaults to None.
        """
        original = self.read_lines(l)[0]
        print(original)

        if k is None:
            k = original[0]

        if t is None:
            t = original[1]

        if v is None:
            v = original[2]

        with FileEdit(self.file) as f:
            f.replace_line(l + 1, self.dummy(k, t, v))  # FileEdit starts line no at 1, readlines uses slices, thus, 0

    def replace_pattern(self, pattern, replacement, secure=True):
        """
        Replaces all occurrences of the given pattern with the replacement string.
        
        Notes
        -----
        This method will iterate over several file object iterators.
        
        Parameters
        ----------
        pattern: str
            Pattern to be replaced.
            
        replacement: str
            Replacement for pattern.
            
        secure: bool
            If set True, an exception will be raised if the replacement string contains the CSV delimiting character.
            Defaults to True.
        """
        if secure and self.delimiter in replacement:
            raise IOError('Invalid character. The replacement string cannot contain the CSV delimiting character!')

        with FileEdit(self.file) as f:
            f.replace_pattern(pattern, replacement)

    # Log reading functions
    def read_lines(self, *lines, greedy=True):
        """
        Reads all lines matching the line numbers passed as arguments from the current log file. Invalid line numbers
        are ignored. If set to greedy, this function will return everything if no valid line number was passed.
        
        Notes
        -----
        This method will skip over most if the file object iterator.
        
        Parameters
        ----------
        lines: integer as *args
            Line numbers of the lines to be read. Invalid line numbers are omitted.
            
        greedy: bool
            If True and no valid line number is passed, everything will be returned.

        Returns
        -------
        list  of [str,str,str]
            List of lines, split into sublists of the pattern key (str), time (str) and value (str).
        """
        # parsing lines.
        # Invalid lines are ignored (they trigger an exception and prevent execution of the else-block which appends l)
        l = []
        for line in lines:
            try:
                n = int(line)

            except (ValueError, TypeError):
                pass  # entering this section will prevent else section from being executed

            else:
                if 0 <= n < unbuffered_linecount(self.file):
                    l.append(n)

        else:
            # the way the iterator slice method from itertools works, every line number needs to be referenced to the
            # start of the iterator, thus, the preceding line number rather than the start of the file object iterator.
            lines = [l[0]] + [l[i]-l[i-1] for i in range(1, len(l))]
            del l

        # reading lines from file. return everything if no line numbers are specified and call is greedy
        with open(self.file, 'r', newline='') as f:
            r = csv.reader(f, delimiter=self.delimiter)

            if not lines and greedy:
                return [[k, t, v] for k, t, v in r]

            else:
                return [next(islice(r, l, None)) for l in lines]

    def read_keys(self, *keys, greedy=True):
        """
        Reads all lines matching the keys passed as arguments from the current log file. Invalid keys are ignored. If 
        set to greedy, this function will return everything if no valid key was passed.
        
        Notes
        -----
        This method will iterate over the whole file object iterator.

        Parameters
        ----------
        keys: str as *args
            keys of the lines to be read. Invalid keys are omitted.

        greedy: bool
            If True and no valid key is passed, everything will be returned.

        Returns
        -------
        list  of [str,str,str]
            List of lines, split into sub lists of the pattern key (str), time (str) and value (str).
        """
        # parsing keys. Invalid keys are ignored
        k = self.extract_keys()
        keys = [key for key in keys if key in k]
        if not keys and greedy:
            keys = k  # translates to 'return everything if list is empty but call is greedy' later

        # sequentially reading rows, adding row to return list if key matches list
        with open(self.file, 'r', newline='') as f:
            r = csv.reader(f, delimiter=self.delimiter)
            return [[k, t, v] for k, t, v in r if k in keys]

    # Miscellaneous functions
    def extract_keys(self):
        """
        Method to extract a list of keys currently used within the log file.

        Returns
        -------
        list of str
            A list of all keys currently used within the log file.
        """
        keys = []
        with open(self.file, 'r', newline='') as f:
            reader = csv.reader(f, delimiter=self.delimiter)

            for k, t, v in reader:
                if k not in keys:
                    keys.append(k)

        return keys

    def find(self, k=None, t=None, v=None, k_tol=0, t_tol=0, v_tol=0):
        """
        Method to find all entries sharing the given values within a certain degree of tolerance.
        
        Parameters
        ----------
        k
            Key (1st. column). Defaults to None.
        t
            Time stamp (2nd. column). Defaults to None.
        v
            Value or Text (3rd. column). Defaults to None.
            
        k_tol: float
            Tolerance for key as a fraction. Defaults to 0.
            
        t_tol: float
            Tolerance for time stamp as a fraction. Defaults to 0.
        
        v_tol: float
            Tolerance for float as a fraction. Defaults to 0.

        Returns
        -------
        list of int
            List of line numbers.
        """
        if all(x is None for x in (k, t, v)):
            raise ValueError('At leat one value needs to be set!')

        for tol in k_tol, t_tol, v_tol:
            if not (0. <= tol <= 1):
                raise ValueError('Tolerances below 0 and above 1 are not possible!')

        def _compare(x, y, tol):
            try:
                x = float(x)
                y = float(y)

            except (ValueError, TypeError):
                x, y = str(x), str(y)
                d = damerau_levenshtein_distance(x, y) / max(len(x), len(y))
                return True if d <= tol else False

            else:
                return True if (y * (1 - tol)) <= x <= (y * (1 + tol)) else False

        with open(self.file, 'r', newline='') as f:  # TODO: Remove, for debugging only
            r = csv.reader(f, delimiter=self.delimiter)
            for x in r:
                print(x)

        with open(self.file, 'r', newline='') as f:
            r = csv.reader(f, delimiter=self.delimiter)

            i, matches = 0, []
            for kr, tr, vr in r:
                k_flag = True if k is None else _compare(kr, k, k_tol)
                t_flag = True if t is None else _compare(tr, t, t_tol)
                v_flag = True if v is None else _compare(vr, v, v_tol)

                if k_flag and t_flag and v_flag:
                    matches.append(i)

                i += 1

        return matches


class LogThread(Log, Thread):
    """
    Log class inheriting from threading.Thread. Logs data added to a buffer queue by devices.DeviceThread objects.
    """
    def __init__(self, data_queue, log_id, file_path, csv_delimiter=';', t0=None):
        """
        Initiates instance of the Log object. As a Queue fed Thread.

        Parameters
        ----------
        data_queue: queue.Queue
            A queue object. Buffering log entries.
        
        log_id: str
            Identifier to help clearly distinguish between different Log instances.

        file_path: str
            File path (including filename) of the log file. Non-existent files and/or directories are created.

        csv_delimiter: str
            Single character delimiter of the CSV dialect used. Defaults to ';'.
            This will be ignored if an existing log file is re-opened (the same that is already used will be use again).

        t0: float or None
            Start time of the log (epoch time) or None to use the current time. Defaults to None.
        """
        super().__init__(log_id, file_path, csv_delimiter=csv_delimiter, t0=t0)
        Thread.__init__(self)
        self.queue = data_queue
        self._active = True

    def run(self):
        """ Internal method taking items from the queue and appending them to the log file. """
        while self._active:
            k, t, v = self.queue.get()
            self.append(k, t - self.t0, v)
            self.queue.task_done()

if __name__ == '__main__':
    print("Open Fermenter Logger Class Copyright (C) 2017  Heinrich Heine University\n"
          "This program comes with ABSOLUTELY NO WARRANTY; for details see documentation.\n"
          "This is free software, and you are welcome to redistribute it under certain conditions;"
          "see 'license.txt' for details.\n")
