#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
Simple GUI for the server application of the Open Fermenter modules.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import api
import csv
from miscellaneous import re_replace_patterns
import os
import re
import subprocess
from tempfile import mkstemp
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as messagebox
import tkinter.filedialog as filedialog

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Prototype'
__version__ = '0.1.7'  # main version, minor version, revision


class GUI(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.master.title('Open Fermenter Client v. {} ({})'.format(__version__, __status__))
        # master.protocol('WM_DELETE_WINDOW', self.dialog_exit)  # TODO: implement dialog_exit: is edited? by hash cmp.

        # predefined variables and control flags
        self.api = None
        self._open_log = True  # will be set False by self._open_close when leaving self.__init__
        self._running = False
        self._edit_mode = None

        # pre-allocating all widget attributes in order of occurrence
        # Settings tab
        self.entry_log = None
        self.open_close = None
        self.label_devices = None
        self.device_list = None
        self.label_device_settings = None
        self.device_settings = None
        self.label_cmd_history = None
        self.cmd_history = None
        self.label_manual_cmd = None
        self.cmd = None
        self.entry_cmd = None
        self.query_cmd = None
        self.start_stop = None
        # labbook tab
        self.var_k = None
        self.entry_k = None
        self.var_t = None
        self.entry_t = None
        self.entry_v = None
        self.clear = None
        self.commit = None
        self.manual_entries = None
        # plotting tab
        self.plotting_scripts = None
        self.plotting_values = None
        self.var_a = None
        self.script_argv = None
        self.call_script = None

        # Division of main view in tabs and filling them with widgets
        self.tabs = ttk.Notebook(self)
        self.settings = ttk.Frame(self.tabs)
        self.labbook = ttk.Frame(self.tabs)
        self.plotting = ttk.Frame(self.tabs)

        self.tabs.add(self.settings, text='Settings')
        self.tabs.add(self.labbook, text='Manual log')
        self.tabs.add(self.plotting, text='Plotting')
        self.tabs.pack()

        self.create_settings_widgets()
        self.create_labbook_widgets()
        self.create_plotting_widgets()

        # Setting initial state by executing code for closing logs, clearing input forms, etc.
        self._open_close()
        self._clear()

    # Methods to create widgets
    def create_settings_widgets(self):
        header = tk.Frame(self.settings)
        entry_log = tk.StringVar(self.settings)
        tk.Label(header, text='Log file:').pack(side='left')
        self.entry_log = tk.Entry(header, textvariable=entry_log)
        self.entry_log.bind('<Double-Button-1>', self._select_log)
        self.entry_log.focus_set()
        self.entry_log.pack(side='left', expand=True, fill='x', padx=3, pady=3)
        self.open_close = tk.Button(header, text='Open', command=self._open_close)
        self.open_close.pack(side='left', padx=3, pady=3)
        header.pack(fill='x')

        center = tk.Frame(self.settings)
        devices_overview = tk.Frame(center)
        devices_settings = tk.Frame(center)
        devices_commands = tk.Frame(center)

        self.label_devices = tk.Label(devices_overview, text='Devices:')
        self.label_devices.pack()
        ts = tk.Scrollbar(devices_overview)
        self.device_list = tk.Listbox(devices_overview, yscrollcommand=ts.set, exportselection=False)
        ts.config(command=self.device_list.yview)
        self.device_list.pack(side='left', fill='y', padx=3, pady=3)
        ts.pack(side='left', fill='y', padx=3, pady=3)

        self.label_device_settings = tk.Label(devices_settings, text='Device setting:')
        self.label_device_settings.pack()
        ts = tk.Scrollbar(devices_settings)
        self.device_settings = tk.Text(devices_settings, yscrollcommand=ts.set, state='disabled')
        ts.config(command=self.device_settings.yview)
        self.device_settings.pack(side='left', fill='y', padx=3, pady=3)
        ts.pack(side='left', fill='y', padx=3, pady=3)

        self.label_cmd_history = tk.Label(devices_commands, text='CMD history:')
        self.label_cmd_history.pack()
        ts = tk.Scrollbar(devices_commands)
        self.cmd_history = tk.Text(devices_commands, yscrollcommand=ts.set, state='disabled')
        ts.config(command=self.cmd_history.yview)
        self.cmd_history.pack(side='left', fill='y', padx=3, pady=3)
        ts.pack(side='left', fill='y', padx=3, pady=3)

        devices_overview.pack(side='left', fill='y', expand=True)
        devices_settings.pack(side='left', fill='y', expand=True)
        devices_commands.pack(side='left', fill='y', expand=True)
        center.pack(fill='both', expand=True)

        footer = tk.Frame(self.settings)
        self.label_manual_cmd = tk.Label(footer, text='Manual command:')
        self.label_manual_cmd.pack(side='left')
        self.cmd = tk.StringVar()
        self.cmd.trace_add('write', self._cmd_activate)
        self.entry_cmd = tk.Entry(footer, textvariable=self.cmd, state='disabled')
        self.query_cmd = tk.Button(footer, text='send', command=self._query_cmd)
        self.query_cmd.config(state='disabled', background='lightgrey')
        self.start_stop = tk.Button(footer, text='Start OFS', command=self._start_stop, state='disabled')

        self.entry_cmd.pack(side='left', fill='x', expand=True, padx=3, pady=3)
        self.query_cmd.pack(side='left', padx=3, pady=3)
        self.start_stop.pack(side='left', padx=3, pady=3)
        footer.pack(fill='x')

    def create_labbook_widgets(self):
        self.tabs.tab(1, state='disabled')
        left = tk.Frame(self.labbook)
        k = tk.Frame(left)
        t = tk.Frame(left)
        v = tk.Frame(left)
        b = tk.Frame(left)

        tk.Label(k, text='Key:').pack()
        self.var_k = tk.StringVar()
        self.var_k.trace_add('write', self._commit_state)
        self.entry_k = ttk.Combobox(k, textvariable=self.var_k, postcommand=self._k_postcommand)
        self.entry_k.pack(side='left', fill='x', expand=True, padx=3, pady=3)

        tk.Label(t, text='Time:').pack()
        self.var_t = tk.StringVar()
        self.var_t.trace_add('write', self._commit_state)
        self.entry_t = tk.Entry(t, textvariable=self.var_t)
        self.entry_t.pack(side='left', fill='x', expand=True, padx=3, pady=3)
        tk.Button(t, text='now', command=lambda: self.var_t.set(self.api.runtime())).pack(side='left', padx=3, pady=3)

        tk.Label(v, text='Value:').pack()
        ts = tk.Scrollbar(v)
        self.entry_v = tk.Text(v, yscrollcommand=ts.set)
        ts.config(command=self.device_settings.yview)
        self.entry_v.bind('<<Modified>>', self._txt_validate)
        self.entry_v.pack(side='left', fill='y', padx=3, pady=3)
        ts.pack(side='left', fill='y', padx=3, pady=3)

        self.clear = tk.Button(b, text='Clear all fields', command=self._clear)
        self.commit = tk.Button(b, text='Add new entry', command=self._commit)
        self.clear.pack(side='left', fill='x', expand=True, padx=3, pady=3)
        self.commit.pack(side='left', fill='x', expand=True, padx=3, pady=3)

        k.pack(fill='x')
        t.pack(fill='x')
        v.pack(fill='x')
        b.pack(fill='x')
        left.pack(side='left')

        right = tk.Frame(self.labbook)
        self.create_table(right, 'manual_entries')
        self.manual_entries.config(selectmode='browse')
        self.manual_entries.bind('<Double-Button-1>', lambda event: self._edit(self.api.meta_log, event))
        right.pack(side='left', fill='both', expand=True)

    def create_plotting_widgets(self):
        header = tk.Frame(self.plotting)
        plotting_scripts = tk.Frame(header)
        plotting_values = tk.Frame(header)

        tk.Label(plotting_scripts, text='Plotting scripts:').pack()
        ts = tk.Scrollbar(plotting_scripts)
        self.plotting_scripts = tk.Listbox(plotting_scripts, yscrollcommand=ts.set, exportselection=False)
        ts.config(command=self.plotting_scripts.yview)
        self.plotting_scripts.pack(side='left', fill='both', expand=True, padx=3, pady=3)
        ts.pack(side='left', fill='y', padx=3, pady=3)

        self.create_table(plotting_values, 'plotting_values')  # registers attribute, does not overwrite local variable!

        plotting_scripts.pack(side='left', fill='both', expand=True)
        plotting_values.pack(side='left', fill='both', expand=True)
        header.pack(fill='both', expand=True)

        footer = tk.Frame(self.plotting)
        tk.Label(footer, text='Additional arguments:').pack(side='left')
        self.var_a = tk.StringVar()
        self.var_a.trace_add('write', self._script_argv)
        self.script_argv = tk.Entry(footer, textvariable=self.var_a)
        self.call_script = tk.Button(footer, text='Call script', command=self._call_script)
        self.script_argv.pack(side='left', fill='x', expand=True, padx=3, pady=3)
        self.call_script.pack(side='left', padx=3, pady=3)
        footer.pack(fill='x')

    # Button methods
    def _select_log(self, event=None):
        file = filedialog.askopenfile('r')
        file.close()
        self.entry_log.delete(0, 'end')
        self.entry_log.insert(0, file.name)

    def _open_close(self):
        if self._open_log:
            try:
                self.api.__exit__(None, None, None)
            except AttributeError:
                pass
            self.device_list.delete(0, 'end')
            self.tabs.tab(1, state='disabled')
            self.tabs.tab(2, state='disabled')
            self.entry_log.bind('<Double-Button-1>', self._select_log)
            self.entry_log.config(state='normal')
            self.open_close.config(text='Open')
            self.label_devices.config(foreground='grey')
            self.label_device_settings.config(foreground='grey')
            self.label_cmd_history.config(foreground='grey')
            self.label_manual_cmd.config(foreground='grey')
            self.device_list.config(state='disabled', background='lightgrey')
            self.device_settings.config(background='lightgrey')
            self.cmd_history.config(background='lightgrey')
            self.start_stop.config(state='disabled')
            self.commit.config(state='disabled')
            self._open_log = False

        else:
            scripts = [script for _, _, directory in os.walk('plotting_scripts') for script in directory]
            for script in scripts:
                self.plotting_scripts.insert('end', script)
            if bool(scripts):
                self.plotting_scripts.select_set(0)

            self.api = api.API(self.entry_log.get())  # , self.csv_delimiter, self.t0)
            patterns = {'<N>': '32', '<delimiter>': self.api.meta_log.delimiter}
            self._keys_re = re_replace_patterns(r'^\w{0,<N>}(?! <delimiter>)$', patterns)
            self.tabs.tab(1, state='normal')
            self.tabs.tab(2, state='normal')
            self._plotting_values()
            self.entry_log.unbind('<Double-Button-1>')
            self.entry_log.config(state='disabled')
            self.open_close.config(text='Close')
            self.label_devices.config(foreground='black')
            self.label_device_settings.config(foreground='black')
            self.label_cmd_history.config(foreground='black')
            self.label_manual_cmd.config(foreground='black')
            self.device_list.config(state='normal', background='white')
            self.device_settings.config(background='white')
            self.cmd_history.config(background='white')
            self.start_stop.config(state='normal')
            self.commit.config(state='normal')
            self._open_log = True

    def _device_settings(self, event=None):
        device = self.api[self.device_list.get(self.device_list.curselection())]
        self.device_settings.config(state='normal')
        self.device_settings.delete('1.0', 'end')
        self.device_settings.insert('end', 'device_id:\t{}\n'.format(device.id))
        for k, v in device.serial_connection.items():
            self.device_settings.insert('end', '\t{}:\t{}\n'.format(k, v))
        self.device_settings.insert('end', 'measurement_interval:\t{}\n'.format(device.measurement_interval))
        self.device_settings.insert('end', 'line_format_fn:\t{}\n'.format(device.format_fn.__name__))
        self.device_settings.insert('end', 'strikes:\t{}\n'.format(device.strikes))
        self.device_settings.insert('end', 'timeout_read:\t{}\n'.format(device.timeout_read))
        self.device_settings.insert('end', 'timeout_write:\t{}'.format(device.timeout_write))
        self.device_settings.config(state='disabled')

    def _cmd_activate(self, *args):
        self.query_cmd.config(state='normal' if bool(self.entry_cmd.get()) else 'disabled')

    def _query_cmd(self):
        x = self.device_list.get(self.device_list.curselection()), self.entry_cmd.get()
        self.api.query(*x)
        self.cmd_history.config(state='normal')
        self.cmd_history.insert('end', '{}: {}'.format(*x))
        self.cmd_history.config(state='disabled')

    def _start_stop(self):
        if self._running:
            if messagebox.askyesno('Stop OFS', 'The current run will be stopped.\n Continue?'):
                for device_id in [e for e in self.api]:
                    self.api.cancel(device_id)

                self.device_list.unbind('<<ListboxSelect>>')
                self.device_list.delete(0, 'end')
                self.device_settings.config(state='normal')
                self.device_settings.delete('1.0', 'end')
                self.device_settings.config(state='disabled')

                self.start_stop.config(text='Start OFS')
                self.open_close.config(state='normal')
                self.entry_log.config(state='normal')
                self.entry_cmd.config(state='disabled')
                self._running = False

        else:
            self.api.spawn_from_config()
            for device_id in self.api:
                self.device_list.insert('end', device_id)
            if bool(self.api.devices):
                self.device_list.select_set(0)
                self._device_settings()
                self.device_list.bind('<<ListboxSelect>>', self._device_settings)

            self.reload_table(self.manual_entries, [self.api.meta_log.file])
            self.start_stop.config(text='Stop OFS')
            self.open_close.config(state='disabled')
            self.entry_log.config(state='disabled')
            self.entry_cmd.config(state='normal')
            self._running = True

    def _k_postcommand(self):
        keys = []
        for log in self.api.meta_log, self.api.data_log:
            try:
                keys += log.extract_keys()
            except ValueError:
                pass

        keys = list(set(keys))
        keys.sort()
        self.entry_k.config(values=keys)

    def _txt_validate(self, event):
        flag = self.entry_v.edit_modified()
        if flag:
            txt = self.entry_v.get('1.0', 'end-1c')

            if txt and self.api.meta_log.delimiter not in txt:
                self.entry_v.config(background='white')

            else:
                pos = ['1.0 + {} chars'.format(i) for m in re.finditer(self.api.meta_log.delimiter, txt) for i in
                       (m.start(), m.end())]
                if bool(pos):
                    self.entry_v.tag_add('illegal_character', *pos)
                self.entry_v.tag_config('illegal_character', background='orange')

            self._commit_state()
            self.entry_v.edit_modified(False)

    def _commit_state(self, *args):
        k = self.entry_k.get()
        if k in self.entry_k['values']:
            c = 'black'
            k = True
        elif self._keys_re.match(k) is None:
            c = 'orange'
            k = False
        else:
            c = 'darkgreen'
            k = True

        try:
            float(self.entry_t.get())
        except (ValueError, TypeError):
            t = False
        else:
            t = True

        v = self.entry_v.get('1.0', 'end-1c')
        v = True if v and self.api.meta_log.delimiter not in v else False

        self.commit.config(state='normal' if k and t and v else 'disabled')

        self.entry_k.config(foreground=c)  # workaround: background=c is ignored for some reason
        self.entry_t.config(background='white' if t else 'orange')

    def _edit(self, log, event=None):
        selection = event.widget.selection()
        if bool(selection):
            data = event.widget.item(selection)
            data = (data['text'], data['values'][0], data['values'][1])
            self._edit_mode = log.find(*data)[0]

            self.entry_k.delete(0, 'end')
            self.entry_t.delete(0, 'end')
            self.entry_v.delete('1.0', 'end')
            self.entry_k.insert(0, data[0])
            self.entry_t.insert(0, data[1])

            # allowing \n breaks escaping on system level
            # Explicit type conversion to string to avoid implicit conversion to int/float (pythons duck-typing)
            self.entry_v.insert('1.0', data[2].__str__().replace('<br>', '\n'))

            self.commit.config(text='Edit selected entry')

    def _clear(self):
        self._edit_mode = None
        self.entry_k.delete(0, 'end')
        self.entry_t.delete(0, 'end')
        self.entry_v.delete('1.0', 'end')
        self.entry_t.config(background='white')
        self.commit.config(text='Add new entry', state='disabled')

    def _commit(self):
        k = self.entry_k.get()
        t = self.entry_t.get()
        v = self.entry_v.get('1.0', 'end-1c').replace('\n', '<br>')  # allowing \n breaks escaping on system level

        if self._edit_mode is None:
            self.api.meta_log.append(k, t, v)
        else:
            self.api.meta_log.replace_row(self._edit_mode, k, t, v)

        self.reload_table(self.manual_entries, [self.api.meta_log.file])
        self._clear()

    def _plotting_values(self):
        self.reload_table(self.plotting_values, [self.api.meta_log.file, self.api.data_log.file])
        self.after(500, func=self._plotting_values)

    def _script_argv(self):
        argv = self.script_argv.get()
        sequences = ('\n', '\t')
        self.call_script.config(state='disabled' if any(s in argv for s in sequences) else 'normal')

    def _call_script(self):
        data = [self.plotting_values.item(i) for i in self.plotting_values.selection()]

        temp_files = {}
        for k, _, _ in data:
            if k not in temp_files.keys():
                fd, path = mkstemp(text=True)
                fobj = open(path, 'a', newline='')
                writer = csv.writer(fobj, delimiter=';')
                temp_files[k] = {'fd': fd, 'path': path, 'fobj': fobj, 'writer': writer}

        for k, t, v in data:
            temp_files[k]['writer'].writerow([t, v])

        for file in temp_files.values():
            file.pop('writer')
            file.pop('fobj').close()

        script = self.plotting_scripts.get(self.plotting_scripts.curselection())
        argv = self.script_argv.get()
        arguments = ' '.join(file['path'] for file in temp_files.values()) + ' ' + argv

        try:
            subprocess.run([script, arguments])

        except:  # Fixme: catch the specific exception thrown by subprocess.run rather than everything!
            messagebox.showerror(script, 'Running {} with the selected data and\n\t"{}"\n'
                                         'as arguments raised an exception!\n\n'
                                         'Task exited unfinished.'.format(script, argv))
        finally:
            for file in temp_files.values():
                os.close(file['fd'])

    # Methods related to the tables
    def create_table(self, parent, name):
        ts = tk.Scrollbar(parent)
        table = ttk.Treeview(parent, yscrollcommand=ts.set)
        setattr(self, name, table)

        table['columns'] = ('runtime', 'value')
        table.heading("#0", text='Key', anchor='w', command=lambda: self.sort_table(getattr(self, name), column=0))
        table.heading('runtime', text='runtime', command=lambda: self.sort_table(getattr(self, name), column=1))
        table.heading('value', text='Value', command=lambda: self.sort_table(getattr(self, name), column=2))

        table.column("#0", anchor="w")
        table.column('runtime', anchor='center', width=100)
        table.column('value', anchor='center', width=100)

        table.pack(side='left', fill='both', expand=True, padx=3, pady=3)
        ts.config(command=table.yview)
        ts.pack(side='left', fill='y', padx=3, pady=3)
        return table

    @staticmethod
    def reload_table(table, file_paths):
        table.delete(*table.get_children())

        for file_path in file_paths:
            with open(file_path) as f:
                delimiter = csv.Sniffer().sniff(f.readline()).delimiter

            with open(file_path, newline='') as f:
                data = csv.reader(f, delimiter=delimiter)
                for k, t, v in data:
                    table.insert('', 'end', text=k, values=(t, v))

    @staticmethod
    def sort_table(table, column):
        data = [table.item(i) for i in table.get_children()]
        data = [(row['text'], row['values'][0], row['values'][1]) for row in data]
        table.delete(*table.get_children())
        data = sorted(data, key=lambda x: x[column])
        for k, t, v in data:
            table.insert('', 'end', text=k, values=(t, v))

# initiating gui
if __name__ == '__main__':
    print("Open Fermenter GUI Class Copyright (C) 2017  Heinrich Heine University\n"
          "This program comes with ABSOLUTELY NO WARRANTY; for details see documentation.\n"
          "This is free software, and you are welcome to redistribute it under certain conditions;"
          "see 'license.txt' for details.\n")
    root = tk.Tk()
    gui = GUI(root)
    gui.mainloop()
