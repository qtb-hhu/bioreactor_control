#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6.1

"""
Installer for the external resources of the Open Fermenter Server software.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
from urllib import request

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Prototype'
__version__ = '0.0.1'  # main version, minor version, revision

# Download external resources from GitLab
external_resources = {
    'fileedit.py': 'https://gitlab.com/snippets/1663140/raw',
    'clock.py': 'https://gitlab.com/snippets/1663081/raw',
    'perpetualtimers.py': 'https://gitlab.com/snippets/1663125/raw',
    'unbuffered_linecount.py': 'https://gitlab.com/snippets/1663307/raw'
}

for file, url in external_resources.items():
    request.urlretrieve(url, file)


# Create directory for plotting scripts and downloads them from various locations
try:
    os.mkdir('plotting_scripts')
except FileExistsError:
    pass

external_scripts = {}
for file, url in external_scripts.items():
    request.urlretrieve(url, 'plotting_scripts/' + file)
