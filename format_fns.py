#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
Predefined line format functions for the Open Fermenter modules.
If you want to permanently add custom functions it is recommended to append them to this file.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Development'
__version__ = '0.0.0'  # main version, minor version, revision


def delimiter_split(lines, delimiter):
    # standard function for splitting key;time;Value
    return [tuple(e.strip() for e in line.split(delimiter)) for line in lines.split('\n')]


def semicolon_split(lines):
    # alias for delimiter split with fixed delimiter: ';'
    return delimiter_split(lines, ';')


def tab_split(lines):
    # alias for delimiter split with fixed delimiter: '\t'
    return delimiter_split(lines, '\t')


def offgas_fn(lines):
    out = []
    subdevice_names = 'subdevice_a', 'subdevice_b', 'subdevice_c', 'subdevice_d'

    lines = semicolon_split(lines)
    for line in lines:
        time = line[-1]
        for key, value in zip(subdevice_names, line):
            out.append((key, time, value))

    return out


def header_parse(lines):
    out = []
    lines = semicolon_split(lines)
    header = lines.pop(0)
    for line in lines:
        time = line.pop[-1]
        for key, value in zip(header, line):
            out.append((key, time, value))

    return out

