# The **Open Fermenter Server** API
The **Open Fermenter Server** consists of the two core elements **devices.py** and **logger.py** which take care of the
communication with the Arduino based modules and the logging thereof. The API was written as a wrapper around those two
components and makes the control of the Devices more convenient for the user.

The API supports use as context manager *e. g.* to wrap around an interactive loop or a GUI (*graphic user interface*) 
but direct instances are perfectly fine, too. The **__exit__* method of the API is registered as **atexit** task on 
initiation of the instance and unregistered of **__exit__** is called after all. Thus, a proper shutdown of the server
software is always ensured.

## Initiation of the server side fermenter control
First, import the **Open Fermenter System** API from api.py and create an instance. The API needs to know where to save 
the log file and thus requires a file path as single argument. Optional arguments for the delimiter of the CSV log files
and the reference times are omitted here, thus, the API will assume the standard values.

```python
from api import API
ofs = API('example_log.csv')
```

Now that we've initiated the API we need to initiate the devices as well. We could automaticall read them from a YAML
encoded configuration file by calling...

```python
ofs.spawn_from_config('CONFIG_FILENAME.yaml')
```

..but for now we'll do it manually.

```python
ofs.spawn('DummyDevice_1', {}, ofs.runtime(), 30, lambda x: x.split(';'))
ofs.spawn('DummyDevice_2', {}, ofs.runtime(), 60, lambda x: x.split(';'))
ofs.spawn('DummyDevice_3', {}, ofs.runtime(), 90, lambda x: x.split(';'))
```

Now that we've created three dummy devices and ensures that everything is properly connected it is time to start the 
devices' threads. Iterating over the instance will yield the devices' IDs.

```python
for device_id in ofs:
    ofs.start(device_id)
```

## Manual interference
Now we will assume that the experiment has been running for a few hours and manual interference is required. e.g. we
need to innoculate the fermenter after all the probes have been tested, thoroughly. Let's assume for some reason we also
 want all devices to pause until we're done.
 
Pausing and resuming devices is straight forward. The methods pause and resume will take care of everything and also add
 a note to the log. With the devices paused we now are free to perform our task(s). 

```python
# pausing all devices
for device_id in ofs:
    ofs.pause(device_id)

# we've done some unholy things to the fermenter
ofs.append('meta', None, 'Inoculum')  # A note for the log. Always recommended!

ofs.query('DummyDevice_2', 'RATE10')  # let's also sends a random command to one device, just for good measure

# and finally resume everything
for device_id in ofs:
    ofs.resume(device_id)
```

## Shutdown
Shutdown of the OFS API is also straight forward. Just exit the context manager, exit the programm or call the __exit__ 
method. It will take care of all the tasks required to stop all devices, and close and save all logs. Keep in mind that 
once stopped, threads cannot be resumed. Thus, in order to reuse the instance to restart the server process with exactly
 the same settings we need to temporarily store the content of the *devices* attribute, replace it with an empty 
dictionary and then use the attributes of the temporary copy to re-respawn the device threads.

```python
old, ofs.devices = ofs.devices, {}
for device in ofs:
    ofs.spawn(device.id, device.serial_connection, device.t0, device.interval, device.format_fn)

del old  # deletion of old is required to avoid ref. cycles
```