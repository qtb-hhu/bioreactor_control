#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.6

"""
Simple helper functions that do not fit anywhere else.

Copyright (C) 2017 Heinrich Heine University

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re

__author__ = 'Philipp Norf'
__copyright__ = 'copyright (C) 2017 Heinrich Heine University'
__license__ = 'AGPL'
__maintainer__ = 'Philipp Norf'
__email__ = 'philipp.norf@uni-duesseldorf.de'
__status__ = 'Development'
__version__ = '0.0.0'  # main version, minor version, revision


def re_replace_patterns(string, patterns):
    """
    Creates an regular expression by replacing patterns in a string (like the format method of strings).

    Parameters
    ----------
    string: str
        String containing patterns to be replaced.

    patterns: dict
        Dictionary containing replacement patterns (keys) and replacements (values).

    Returns
    -------
    Compiled regular expression
    """
    # building a RE to validate keys
    for pattern, replacement in patterns.items():
        string = re.sub(pattern, replacement, string)

    return re.compile(string)
